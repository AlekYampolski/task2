import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { GetCurrencies, ECurrenciesActions } from './store/actions/currencies.actions';
import { IAppState } from './store/state/app.state';
import { CurrenciesEffects } from './store/effects/currencies.effect';
import { takeUntil } from 'rxjs/operators';

import { ICurrencies } from './models/currencies.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {
  title = 'task #2';
  cryptList: ICurrencies[];
  destroy$: Subject<boolean> = new Subject<boolean>();
  private isLoaded: boolean;
  private isError: boolean;

  constructor(
    private _store: Store<IAppState>,
    private seeEffects: CurrenciesEffects
    ) {
      this.seeEffects.getCurrencies$.pipe(takeUntil(this.destroy$)) .subscribe(res => {
        this.isLoaded = true;
        if (res.type === ECurrenciesActions.GetCurrenciesError) {
          this.isError = true;
        }
        if (res.type === ECurrenciesActions.GetCurrenciesSuccess) {
          this.cryptList = res.payload;
        }
      });
     }

  ngOnInit() {

    this._store.dispatch(new GetCurrencies());
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }


}
