import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import { ConvertService } from '../../services/convert/convert.service';
import { ICurrencies } from '../../models/currencies.interface';
import { IAppState } from 'src/app/store/state/app.state';
import { SelectCrypto } from '../../store/actions/selection.actions';

@Component({
  selector: 'app-currency-card',
  templateUrl: './currency-card.component.html',
  styleUrls: ['./currency-card.component.scss']
})
export class CurrencyCardComponent implements OnInit {

  @Input() cryptoName: string;
  @Input() cryptoData: ICurrencies[];

  private _cryptoObject: ICurrencies[];

  constructor(
    private _mStates: ConvertService,
    private _store: Store<IAppState>,
    ) {}

  ngOnInit() {

    this._store.subscribe(data => {
      this.cryptoData = data.listOfCurrencies.currencies;

      this._cryptoObject = this.cryptoData.filter(element => {
        return element.type === this.cryptoName;
      });
    });

  }

  onChangeCrypto(val: string) {
    this._store.dispatch(new SelectCrypto(val));
    this._mStates.convertCurrency();
  }

}
