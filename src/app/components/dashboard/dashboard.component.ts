import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';

import { ICurrencies } from '../../models/currencies.interface';
import { ConvertService } from '../../services/convert/convert.service';
import { IAppState } from 'src/app/store/state/app.state';
import { SelectRegular } from '../../store/actions/selection.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {

  private _destroy$: Subject<boolean> = new Subject<boolean>();

  @Input() cryptoList: ICurrencies;
  @ViewChild('inputConverter', {static: false}) inputConverter: ElementRef;

  private regularType: string;
  private cryptoType: string;
  private cryptoTotal = 0;
  private listOfRegularCurrency = ['USD', 'UAH', 'RUB'];

  constructor(
    private mStates: ConvertService,
    private _store: Store<IAppState>
    ) {}

  ngOnInit() {
    this._store.subscribe(res => {
      this.regularType = res.selectedCurrencies.regular;
      this.cryptoType = res.selectedCurrencies.crypto;
    });
  }

  ngAfterViewInit() {
    fromEvent<KeyboardEvent>(this.inputConverter.nativeElement, 'input')
      .pipe(
        takeUntil(this._destroy$)
      )
      .subscribe(item => {
        const element = item.target as HTMLInputElement;
        this.cryptoTotal = +element.value;
        this.mStates.cryptoTotal = +element.value;
        this.mStates.convertCurrency();
      });
  }

  ngOnDestroy() {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }

  changeRegularCurrency(value: string) {
    this.mStates.selectedRegularCurrency = value;
    this._store.dispatch(new SelectRegular(value));
    this.mStates.convertCurrency();
  }

}
