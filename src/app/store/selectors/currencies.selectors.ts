import { createSelector } from '@ngrx/store';

import { IAppState } from '../state/app.state';
import { ICurrenciesState } from '../state/currencies.state';

const currenciesState = (state: IAppState) => state.listOfCurrencies;

export const selectCurrencyList = createSelector(
    currenciesState,
    (state: ICurrenciesState) => state.currencies
);

