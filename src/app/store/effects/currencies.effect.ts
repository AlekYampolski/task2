import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';

import { GetCurrencies, ECurrenciesActions, GetCurrenciesSuccess, GetCurrenciesError } from '../actions/currencies.actions';

import { switchMap, catchError } from 'rxjs/operators';
import { CryptaService } from '../../services/crypta/crypta.service';
import { ICurrencies } from '../../models/currencies.interface';
import { of } from 'rxjs';

@Injectable()
export class CurrenciesEffects {
    @Effect()
    getCurrencies$ = this._actions$.pipe(
        ofType<GetCurrencies>(ECurrenciesActions.GetCurrencies),
        switchMap( () => this._currencyService.getAllCrypts()),
        switchMap((currency: ICurrencies[]) => {
            return of(new GetCurrenciesSuccess(currency));
        }),
        catchError(() => of(new GetCurrenciesError()))
    );

    constructor(
        private _actions$: Actions,
        private _currencyService: CryptaService
    ) {}
}
