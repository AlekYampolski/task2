import { ICurrenciesState, initialCurrenciesState} from './currencies.state';
import { ISelectionState, initialISelectionState } from './selection.state';


export interface IAppState {
    listOfCurrencies: ICurrenciesState;
    selectedCurrencies: ISelectionState;
}

export const initialAppState: IAppState = {
    listOfCurrencies: initialCurrenciesState,
    selectedCurrencies: initialISelectionState
}

export function getInitialState(): IAppState {
    return initialAppState;
}