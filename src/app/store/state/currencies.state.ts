import { ICurrencies } from '../../models/currencies.interface';

export interface ICurrenciesState {
    currencies: ICurrencies[];
}

export const initialCurrenciesState: ICurrenciesState = {
    currencies: [],
}

