import { ISelection } from '../../models/selection.interface';

export interface ISelectionState {
    regular: string;
    crypto: string;
}

export const initialISelectionState: ISelectionState = {
    regular: 'UAH',
    crypto: 'BTC'
};
