import { initialCurrenciesState, ICurrenciesState } from '../state/currencies.state';
import { ECurrenciesActions, CurrenciesActions } from '../actions/currencies.actions';

export const currenciesReducers = (
    state = initialCurrenciesState,
    action: CurrenciesActions
): ICurrenciesState => {
    switch (action.type) {
        case ECurrenciesActions.GetCurrenciesSuccess: {
            return {
                ...state,
                currencies: action.payload
            };
        }

        case ECurrenciesActions.GetCurrenciesError: {
            return {
                    ...state,
                    currencies: []
            };
        }

        default:
            return state;
    }
};
