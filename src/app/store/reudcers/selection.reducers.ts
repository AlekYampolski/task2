import { ESelectionActions, SelectionAction } from '../actions/selection.actions';
import { initialISelectionState, ISelectionState} from '../state/selection.state';

export const selectionReducers = (
    state = initialISelectionState,
    action: SelectionAction
): ISelectionState => {
    switch (action.type) {
        case ESelectionActions.SelectCrypto: {
            return {
                ...state,
                crypto: action.payload
            };
        }

        case ESelectionActions.SelectRegular: {
            return {
                ...state,
                regular: action.payload
            };
        }

        default: {
            return state;
        }
    }
};
