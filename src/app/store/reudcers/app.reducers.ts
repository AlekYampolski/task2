import { currenciesReducers } from './currencies.reducers';
import { selectionReducers } from './selection.reducers';
import { IAppState } from '../state/app.state';
import { ActionReducerMap } from '@ngrx/store';

export const appReducers: ActionReducerMap<IAppState, any> = {
    listOfCurrencies: currenciesReducers,
    selectedCurrencies: selectionReducers
}