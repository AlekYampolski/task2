import { Action } from '@ngrx/store';

import { ICurrencies } from '../../models/currencies.interface';

export enum ECurrenciesActions {
    GetCurrencies = '[Currencies] Get Currencies Data',
    GetCurrenciesSuccess = '[Currencies] Get Currencies Data Loaded Success',
    GetCurrenciesError = '[Currencies] Get Currencies Data Loaded Error',
}

export class GetCurrencies implements Action {
    readonly type = ECurrenciesActions.GetCurrencies;
}

export class GetCurrenciesSuccess implements Action {
    readonly type = ECurrenciesActions.GetCurrenciesSuccess;

    constructor(public payload: ICurrencies[]) {}
}

export class GetCurrenciesError implements Action {
    readonly type = ECurrenciesActions.GetCurrenciesError;
}

export type CurrenciesActions = GetCurrencies | GetCurrenciesSuccess | GetCurrenciesError;
