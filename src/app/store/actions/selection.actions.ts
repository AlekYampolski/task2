import { Action } from '@ngrx/store';

export enum ESelectionActions {
    SelectCrypto = '[Selection] Select Crypto',
    SelectRegular = '[Selection] Select Rergular'
}

export class SelectCrypto implements Action {
    public readonly type = ESelectionActions.SelectCrypto;

    constructor(public payload: string) {}
}

export class SelectRegular implements Action {
    public readonly type = ESelectionActions.SelectRegular;

    constructor(public payload: string) {}
}

export type SelectionAction = SelectCrypto | SelectRegular;
