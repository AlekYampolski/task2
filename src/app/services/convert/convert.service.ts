import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { IAppState } from 'src/app/store/state/app.state';
import { ICurrencies } from 'src/app/models/currencies.interface';

@Injectable({
  providedIn: 'root'
})
export class ConvertService {
  public selectedCryptoCurrency: string;
  public selectedRegularCurrency: string;
  public regularTotal = 0;
  public cryptoTotal = 0;
  public cryptoData: ICurrencies[];

  constructor(
    private _store: Store<IAppState>

    ) {
      this._store.subscribe(state => {
        this.selectedRegularCurrency = state.selectedCurrencies.regular;
        this.selectedCryptoCurrency = state.selectedCurrencies.crypto;
        this.cryptoData = state.listOfCurrencies.currencies;
      });
    }

  convertCurrency() {
    const values =  this.cryptoData.find(element => element.type === this.selectedCryptoCurrency);
    this.regularTotal = values.value[this.selectedRegularCurrency] * this.cryptoTotal;
  }
}
