import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICurrencies } from '../../models/currencies.interface';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private pathCrypto = 'https://min-api.cryptocompare.com/data/price?fsym=';

  constructor( private _http: HttpClient ) { }


  getCryptocurrencyByName(name: string, currencyTypes: string): Observable<ICurrencies> {
    return this._http.get<ICurrencies>(`${this.pathCrypto}${name}&tsyms=${currencyTypes}`);
  }

}

