import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ICurrencies } from '../../models/currencies.interface';

@Injectable({
  providedIn: 'root'
})
export class CryptaService {
  private _cryptList = ['BTC', 'ETH', 'XRP'];
  private _currencyTypes = 'USD,UAH,RUB';
  public cryptoData: Array<ICurrencies>;

  constructor(
    private api: HttpService,
  ) {}

  getAllCrypts(): Observable<Array<ICurrencies>> {
    const requests = this._cryptList.map(cryptName => {
      return this.api.getCryptocurrencyByName(cryptName, this._currencyTypes).pipe(
        map(value => {
          return {
            type: cryptName,
            value
          };
        })
      );
    });
    // tslint:disable-next-line: deprecation
    return forkJoin(...requests);
  }

}
