export interface ICurrencies {
  type: string;
  value: {
      USD: number,
      UAH: number,
      RUB: number
  };
}
