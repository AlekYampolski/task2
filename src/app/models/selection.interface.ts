export interface ISelection {
    regular: string;
    crypto: string;
}